gitlabhq_production=# \d+ web_hook_logs*
                                                                  Table "public.web_hook_logs"
         Column         |            Type             | Collation | Nullable |                  Default                  | Storage  | Stats target | Description
------------------------+-----------------------------+-----------+----------+-------------------------------------------+----------+--------------+-------------
 id                     | integer                     |           | not null | nextval('web_hook_logs_id_seq'::regclass) | plain    |              |
 web_hook_id            | integer                     |           | not null |                                           | plain    |              |
 trigger                | character varying           |           |          |                                           | extended |              |
 url                    | character varying           |           |          |                                           | extended |              |
 request_headers        | text                        |           |          |                                           | extended |              |
 request_data           | text                        |           |          |                                           | extended |              |
 response_headers       | text                        |           |          |                                           | extended |              |
 response_body          | text                        |           |          |                                           | extended |              |
 response_status        | character varying           |           |          |                                           | extended |              |
 execution_duration     | double precision            |           |          |                                           | plain    |              |
 internal_error_message | character varying           |           |          |                                           | extended |              |
 created_at             | timestamp without time zone |           | not null |                                           | plain    |              |
 updated_at             | timestamp without time zone |           | not null |                                           | plain    |              |
Indexes:
    "web_hook_logs_pkey" PRIMARY KEY, btree (id)
    "index_web_hook_logs_on_created_at_and_web_hook_id" btree (created_at, web_hook_id)
    "index_web_hook_logs_on_web_hook_id" btree (web_hook_id)
Foreign-key constraints:
    "fk_rails_666826e111" FOREIGN KEY (web_hook_id) REFERENCES web_hooks(id) ON DELETE CASCADE
Triggers:
    table_sync_trigger_b99eb6998c AFTER INSERT OR DELETE OR UPDATE ON web_hook_logs FOR EACH ROW EXECUTE FUNCTION table_sync_function_29bc99d6db()
Access method: heap

                Sequence "public.web_hook_logs_id_seq"
  Type   | Start | Minimum |  Maximum   | Increment | Cycles? | Cache
---------+-------+---------+------------+-----------+---------+-------
 integer |     1 |       1 | 2147483647 |         1 | no      |     1
Owned by: public.web_hook_logs.id

                                   Partitioned table "public.web_hook_logs_part_0c5294f417"
         Column         |            Type             | Collation | Nullable | Default | Storage  | Stats target | Description
------------------------+-----------------------------+-----------+----------+---------+----------+--------------+-------------
 id                     | bigint                      |           | not null |         | plain    |              |
 web_hook_id            | integer                     |           | not null |         | plain    |              |
 trigger                | character varying           |           |          |         | extended |              |
 url                    | character varying           |           |          |         | extended |              |
 request_headers        | text                        |           |          |         | extended |              |
 request_data           | text                        |           |          |         | extended |              |
 response_headers       | text                        |           |          |         | extended |              |
 response_body          | text                        |           |          |         | extended |              |
 response_status        | character varying           |           |          |         | extended |              |
 execution_duration     | double precision            |           |          |         | plain    |              |
 internal_error_message | character varying           |           |          |         | extended |              |
 updated_at             | timestamp without time zone |           | not null |         | plain    |              |
 created_at             | timestamp without time zone |           | not null |         | plain    |              |
Partition key: RANGE (created_at)
Indexes:
    "web_hook_logs_part_0c5294f417_pkey" PRIMARY KEY, btree (id, created_at)
    "index_web_hook_logs_part_on_created_at_and_web_hook_id" btree (created_at, web_hook_id)
    "index_web_hook_logs_part_on_web_hook_id" btree (web_hook_id)
Foreign-key constraints:
    "fk_rails_bb3355782d" FOREIGN KEY (web_hook_id) REFERENCES web_hooks(id) ON DELETE CASCADE
Partitions: gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_000000 FOR VALUES FROM (MINVALUE) TO ('2022-07-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202207 FOR VALUES FROM ('2022-07-01 00:00:00') TO ('2022-08-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202208 FOR VALUES FROM ('2022-08-01 00:00:00') TO ('2022-09-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202209 FOR VALUES FROM ('2022-09-01 00:00:00') TO ('2022-10-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202210 FOR VALUES FROM ('2022-10-01 00:00:00') TO ('2022-11-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202211 FOR VALUES FROM ('2022-11-01 00:00:00') TO ('2022-12-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202212 FOR VALUES FROM ('2022-12-01 00:00:00') TO ('2023-01-01 00:00:00'),
            gitlab_partitions_dynamic.web_hook_logs_part_0c5294f417_202301 FOR VALUES FROM ('2023-01-01 00:00:00') TO ('2023-02-01 00:00:00')

             Partitioned index "public.web_hook_logs_part_0c5294f417_pkey"
   Column   |            Type             | Key? | Definition | Storage | Stats target
------------+-----------------------------+------+------------+---------+--------------
 id         | bigint                      | yes  | id         | plain   |
 created_at | timestamp without time zone | yes  | created_at | plain   |
primary key, btree, for table "public.web_hook_logs_part_0c5294f417"

               Index "public.web_hook_logs_pkey"
 Column |  Type   | Key? | Definition | Storage | Stats target
--------+---------+------+------------+---------+--------------
 id     | integer | yes  | id         | plain   |
primary key, btree, for table "public.web_hook_logs"
